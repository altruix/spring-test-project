angular.module('todomvc', [ 'ngRoute', 'ngResource' ])
.config(function($routeProvider,
$httpProvider) {
	$routeProvider
	.when('/', {
    	    templateUrl : 'login.html',
    	    controller : 'navigation'
    	})
	.when('/home', {
		templateUrl : 'home.html',
		controller : 'TodoCtrl'
	})
	.when('/home/active', {
    		templateUrl : 'home.html',
    		controller : 'TodoCtrl'
    	})
	.when('/home/completed', {
    		templateUrl : 'home.html',
    		controller : 'TodoCtrl'
    	})
	.otherwise('/');
	$httpProvider.defaults.headers.common['X-Requested-With'] =
	    'XMLHttpRequest';
})
.controller(
     'navigation',
     function($rootScope, $scope, $http, $location, $route, $injector) {
         $scope.tab = function(route) {
             return $route.current && route === $route.current.controller;
         };
         $scope.credentials = {};
         $scope.login = function() {
            $http.post('login', $.param($scope.credentials), {
                headers : {
                  "content-type" : "application/x-www-form-urlencoded"
                }})
                .then(function(response){
                    // Login success
                     $scope.error = false;
                     $rootScope.authenticated = true;
                     $location.path("/home");
                }, function() {
                    // Login failure
                    $scope.error = true;
                    $rootScope.authenticated = false;
                    $location.path("/");
               });
            return false;
        };
        this.authenticated = function() {
            console.log("app.authenticated: " + app.authenticated);
            console.log("$rootScope.authenticated: " + $rootScope.authenticated);
            return app.authenticated;
         };
         $scope.logout = function() {
             $http.post('logout', {}).success(function() {
                 $rootScope.authenticated = false;
                 $location.path("/");
             }).error(function(data) {
                 console.log("Logout failed")
                 $rootScope.authenticated = false;
             });
         }
     });
