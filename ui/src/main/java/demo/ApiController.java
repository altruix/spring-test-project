package demo;

import com.fasterxml.jackson.databind.ObjectMapper;
import demo.model.todo.Todo;
import demo.model.todo.TodoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@Controller
@RestController
@RequestMapping("/api")
public class ApiController {
    private final static Logger LOGGER = LoggerFactory.getLogger(ApiController.class);

    @Autowired
    private TodoRepository todoRepository;

    @RequestMapping(value = "todos/{id}", method = RequestMethod.DELETE)
    public String deleteTodo(@PathVariable("id") final long id) {
        todoRepo().delete(id);
        return "";
    }

    protected TodoRepository todoRepo() {
        return this.todoRepository;
    }

    @RequestMapping(value = "todos", method = RequestMethod.DELETE)
    public String deleteCompleted() {
        final CustomUserDetails user = this.loggedInUser();
        if (user != null) {
            final String userName = user.getUserName();
            final List<Todo> completedItems = todoRepo()
                    .findByUserNameAndCompleted(userName, true);
            todoRepo().delete(completedItems);
        }
        return "";
    }

    @RequestMapping(value = "todos/{id}", method = RequestMethod.PUT)
    public String updateTodo(@PathVariable("id") final long id,
                             @RequestBody final String txt) {
        final Todo existingTodo = todoRepo().getOne(id);
        if (existingTodo != null) {
            final ObjectMapper mapper = new ObjectMapper();
            try {
                final Map data = mapper.readValue(txt, Map.class);
                final String newTitle = (String) data.get("title");
                final boolean newCompleted = (boolean) data.get("completed");
                existingTodo.setTitle(newTitle);
                existingTodo.setCompleted(newCompleted);
                todoRepo().save(existingTodo);
                return mapper.writeValueAsString(existingTodo);
            } catch (final IOException exception) {
                LOGGER.error("createTodo", exception);
            }
        }
        return "";
    }

    @RequestMapping(value = "todos", method = RequestMethod.GET)
    public String getTodos(@RequestBody final String txt) {
        return "";
    }

    @RequestMapping(value = "todos", method = RequestMethod.POST)
    public String createTodo(@RequestBody final String txt) {
        final ObjectMapper mapper = new ObjectMapper();
        try {
            final Map data = mapper.readValue(txt, Map.class);
            final String title = (String) data.get("title");
            final boolean completed = (boolean) data.get("completed");
            final CustomUserDetails curUser = this.loggedInUser();
            if (curUser != null) {
                final String userName = curUser.getUsername();
                final Todo todo = new Todo();
                todo.setTitle(title);
                todo.setCompleted(completed);
                todo.setUserName(userName);
                todoRepository.save(todo);
                return mapper.writeValueAsString(todo);
            }
        } catch (final IOException exception) {
            LOGGER.error("createTodo", exception);
        }

        return "";
    }

    protected CustomUserDetails loggedInUser() {
        if (SecurityContextHolder.getContext() == null) {
            return null;
        }
        if (SecurityContextHolder.getContext().getAuthentication() == null) {
            return null;
        }
        final Object principal = SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getPrincipal();
        if (principal == null) {
            return null;
        }
        if (principal instanceof CustomUserDetails) {
            return (CustomUserDetails) principal;
        }
        return null;
    }

}