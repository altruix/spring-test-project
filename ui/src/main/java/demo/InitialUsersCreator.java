package demo;

import demo.model.users.Users;
import demo.model.users.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class InitialUsersCreator implements ApplicationRunner {
    @Autowired
    private UsersRepository usersRepository;

    @Override
    public void run(final ApplicationArguments args) throws
            Exception {
        final Users user = new Users();
        user.setPassword("testpass");
        user.setUserName("testuser");
        user.setActive(true);
        usersRepository.save(user);
    }
}
