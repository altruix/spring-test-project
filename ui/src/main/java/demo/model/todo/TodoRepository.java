package demo.model.todo;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TodoRepository extends JpaRepository<Todo, Long> {
    // Optional<Todo> findByUserName(final String username);
    List<Todo> findByUserNameAndCompleted(
            final String username,
            final boolean completed
    );
}
