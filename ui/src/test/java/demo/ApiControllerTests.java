package demo;

import demo.model.todo.Todo;
import demo.model.todo.TodoRepository;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.List;

import static org.mockito.Mockito.*;

public class ApiControllerTests {
    @Test
    public void deleteTodo() {
        // Prepare
        final ApiController sut = Mockito.spy(new ApiController());
        final TodoRepository todoRepo = mock(TodoRepository.class);
        Mockito.doReturn(todoRepo).when(sut).todoRepo();
        final long id = 123L;

        // Run method under test
        sut.deleteTodo(id);

        // Verify
        Mockito.verify(todoRepo).delete(id);
    }

    @Test
    public void deleteCompleted() {
        // Prepare
        final ApiController sut = Mockito.spy(new ApiController());
        final TodoRepository todoRepo = mock(TodoRepository.class);
        Mockito.doReturn(todoRepo).when(sut).todoRepo();
        final CustomUserDetails user = mock(CustomUserDetails.class);
        Mockito.doReturn(user).when(sut).loggedInUser();
        final String userName = "Karl";
        when(user.getUserName()).thenReturn(userName);
        final List completedItems = mock(List.class);
        when(todoRepo.findByUserNameAndCompleted(userName, true))
                .thenReturn(completedItems);

        // Run method under test
        sut.deleteCompleted();

        // Verify
        verify(sut).loggedInUser();
        verify(todoRepo).findByUserNameAndCompleted(userName, true);
        verify(todoRepo).delete(completedItems);
    }
    @Test
    public void updateTodo() {
        // Prepare
        final ApiController sut = Mockito.spy(new ApiController());
        final TodoRepository todoRepo = mock(TodoRepository.class);
        Mockito.doReturn(todoRepo).when(sut).todoRepo();
        final Todo todo = new Todo();
        todo.setCompleted(false);
        todo.setTitle("abc");
        final long id = 1L;
        when(todoRepo.getOne(id)).thenReturn(todo);
        when(todoRepo.save(Mockito.any(Todo.class))).thenAnswer(
                (Answer) invocationOnMock -> {
                    final Todo updatedTodo = (Todo)invocationOnMock.getArgument
                            (0);
                    Assert.assertEquals("Test", updatedTodo.getTitle());
                    Assert.assertEquals(true, updatedTodo.isCompleted());
                    Assert.assertEquals(0, updatedTodo.getId());
                    System.out.println("");
                    return null;
                });

        // Run method under test
        sut.updateTodo(1, "{\"title\": \"Test\", \"completed\": true, \"id\": 1}");

        // Verify
        verify(todoRepo).save(Mockito.any(Todo.class));
    }
}
